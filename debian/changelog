osdclock (0.5-26) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.6.0 to 4.7.0.
  * Use wrap-and-sort -at for debian control files.
  * Cleaned up debian patches and fixed hardening build.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 16 Jun 2024 13:07:23 +0200

osdclock (0.5-25) unstable; urgency=medium

  * QA upload.
  * Change to dh sequencer.
    - This includes target build-arch and build-indep.  Closes: #999184
    - Makes it possible to cross-build. Closes: #941435
    - Using default Debian CFLAGS when compiling.
  * New patch: Use destdir, create directories and place man-page
    in correct path. This also makes d/manpages and d/install unnecessary.
  * Change to debhelper-compat.
  * Bump debhelper to 13.
  * Document Rules-Requires-Root.
  * Update Standards-Version to 4.6.0
  * d/copyright:
    - Update to be DEP5 compliant.
    - Add year to upstream copyright holder.

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Mon, 20 Dec 2021 09:30:14 +0100

osdclock (0.5-24) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group. (see #866940)
  * Apply patch from Jari Aalto: (Closes: #664384)
    - Remove deprecated dpatch and upgrade to packaging format "3.0 quilt".
    - Update to Standards-Version to 3.9.3 and debhelper to 9.

 -- Adrian Bunk <bunk@debian.org>  Sat, 08 Jul 2017 14:36:56 +0300

osdclock (0.5-23) unstable; urgency=low

  * New package maintainer (closes: #483810).
  * Standards-Version updated to 3.8.1

 -- Alexandre De Dommelin <adedommelin@tuxz.net>  Sat, 13 Jun 2009 15:53:05 +0200

osdclock (0.5-22) unstable; urgency=low

  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Fri, 20 Jun 2008 09:52:00 +0200

osdclock (0.5-21) unstable; urgency=low

  * Updated to debhelper 7.
  * Rewriting copyright file in machine-interpretable format.
  * Reordering rules file.

 -- Daniel Baumann <daniel@debian.org>  Sat, 31 May 2008 11:36:00 +0200

osdclock (0.5-20) unstable; urgency=low

  * Bumped to new policy.
  * Don't hide make errors in clean target of rules.

 -- Daniel Baumann <daniel@debian.org>  Sun, 23 Dec 2007 16:54:00 +0100

osdclock (0.5-19) unstable; urgency=low

  * Adjusted manpage section.

 -- Daniel Baumann <daniel@debian.org>  Tue, 15 May 2007 15:05:00 +0200

osdclock (0.5-18) unstable; urgency=low

  * New maintainer (Closes: #416960, #416983).
  * Redone debian directory based on current debhelper templates.

 -- Daniel Baumann <daniel@debian.org>  Sun,  1 Apr 2007 09:10:00 +0200

osdclock (0.5-17) unstable; urgency=low

  * Orphan package

 -- Julien Lemoine <speedblue@debian.org>  Sat, 31 Mar 2007 20:19:51 +0200

osdclock (0.5-16) unstable; urgency=low

  * Converted Debian changelog in utf-8
    (it was containing an iso-8859-1 character)
    (Closes: #375148)

 -- Julien Lemoine <speedblue@debian.org>  Tue, 22 Aug 2006 11:33:50 +0200

osdclock (0.5-15) unstable; urgency=low

  * Applied patch from Justin B Rye <jbr@edlug.org.uk> to fix problem
    with man page filename (Closes: #37514)

 -- Julien Lemoine <speedblue@debian.org>  Wed, 28 Jun 2006 21:53:30 +0200

osdclock (0.5-14) unstable; urgency=low

  * Applied patch from Nicolas Salles <salles@crans.ens-cachan.fr> to add
    locale definition in osdclock (Closes: #352739)

 -- Julien Lemoine <speedblue@debian.org>  Mon,  5 Jun 2006 11:56:10 +0200

osdclock (0.5-13) unstable; urgency=low

  * Fixed build failuer (added missing build depends)
    (Closes: #347212)

 -- Julien Lemoine <speedblue@debian.org>  Mon,  9 Jan 2006 15:06:57 +0100

osdclock (0.5-12) unstable; urgency=low

  * Fix x.org transition bug (xlibs-dev removal)
    (Closes: #346903)

 -- Julien Lemoine <speedblue@debian.org>  Mon,  9 Jan 2006 09:31:05 +0100

osdclock (0.5-11) unstable; urgency=low

  * Applied patch from Stephan Windmüller <windy@white-hawk.de>
    Introduces two new options :
    -r locate clock at right side (default: left)
    -w Seconds to wait before first display (default: 0)
    (Closes: 273866)

 -- Julien Lemoine <speedblue@debian.org>  Wed, 29 Sep 2004 20:42:24 +0200

osdclock (0.5-10) unstable; urgency=low

  * Fix default-font for non-ISO8859-1 locales. (closes: #212705)
    Thanks to Philipp Matthias Hahn <pmhahn@debian.org>

 -- Julien Lemoine <speedblue@debian.org>  Tue, 30 Sep 2003 13:54:04 +0200

osdclock (0.5-9) unstable; urgency=low

  * Fixed typos (Closes: #211231)
  * Updated to debian policy 3.6.1

 -- Julien Lemoine <speedblue@debian.org>  Tue, 16 Sep 2003 19:23:01 +0200

osdclock (0.5-8) unstable; urgency=low

  * Rebuild using the new xosd package (2.2.2-2) to fix link errors on S390

 -- Julien Lemoine <speedblue@debian.org>  Wed,  2 Jul 2003 12:32:25 +0200

osdclock (0.5-7) unstable; urgency=low

  * Added -lXinerama to fix undefined references on s390
  * Added build depends on xlibx-dev
  * Updated to debian policy 3.5.10

 -- Julien Lemoine <speedblue@debian.org>  Fri,  6 Jun 2003 17:10:28 +0200

osdclock (0.5-6) unstable; urgency=low

  * Fixed typo in man page (Closes: #194704)
  * Build against latest libxosd
  * Updated to debian policy 3.5.9

 -- Julien Lemoine <speedblue@debian.org>  Tue, 27 May 2003 02:27:27 +0200

osdclock (0.5-5) unstable; urgency=low

  * Added missing options in man and --help (Closes: #183602)

 -- Julien Lemoine <speedblue@debian.org>  Thu,  6 Mar 2003 12:45:43 +0100

osdclock (0.5-4) unstable; urgency=low

  * New Maintainer
  * Fixed char signedness on arm (Closes: #161809)
  * Fixed typo in description (Closes: #140934)
  * Build against latest libxosd (closes: #160804, #181294, #160845)
  * Applyed patch from Matthew Astley <mca-debbts@fruitcake.demon.co.uk>
    (Closes: #160281)
  * Updated Standards-Version to 3.5.8

 -- Julien Lemoine <speedblue@debian.org>  Tue,  4 Mar 2003 14:44:10 +0100

osdclock (0.5-3.1) unstable; urgency=low

  * Non-maintainer upload
  * Build against latest libxosd (closes: #160804)
  * Build-depends on libxosd 1.0.3-1 or more
  * Fix xosd_init invocation to specify number of lines = 2.

 -- Junichi Uekawa <dancer@debian.org>  Sat, 14 Sep 2002 14:16:13 +0900

osdclock (0.5-3) unstable; urgency=low

  * Recompile against the right X libraries. Closes: #95629

 -- Martijn van de Streek <martijn@foodfight.org>  Thu, 14 Jun 2001 13:51:39 +0200

osdclock (0.5-2) unstable; urgency=low

  * Fix override disparity

 -- Martijn van de Streek <martijn@foodfight.org>  Sun, 22 Apr 2001 13:11:57 +0200

osdclock (0.5-1) unstable; urgency=low

  * Initial package

 -- Martijn van de Streek <martijn@foodfight.org>  Wed, 28 Mar 2001 10:30:53 +0200
